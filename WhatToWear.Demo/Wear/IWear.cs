﻿namespace WhatToWear.Demo.Wear
{
    public interface IWear
    {
        IWear NextWear { get; set; }
        void PrintWear();
    }
}
