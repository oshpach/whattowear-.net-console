﻿using System;

namespace WhatToWear.Demo.Wear
{
    public abstract class BaseWear : IWear
    {
        protected string Description;
        public IWear NextWear { get; set; }

        public void PrintWear()
        {
            Console.Write("{0}{1}", Description, NextWear == null ?  "." : ", ");

            if (NextWear != null)
            {
                NextWear.PrintWear();
            }
        }
    }
}
