﻿namespace WhatToWear.Demo.Wear
{
    public class JacketWear : BaseWear
    {
        public JacketWear()
        {
            Description = "Jacket";
        }
    }
}