﻿namespace WhatToWear.Demo.Wear
{
    public class SneakersWear : BaseWear
    {
        public SneakersWear()
        {
            Description = "Sneakers";
        }
    }
}