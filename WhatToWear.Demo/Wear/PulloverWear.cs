﻿namespace WhatToWear.Demo.Wear
{
    public class PulloverWear : BaseWear
    {
        public PulloverWear()
        {
            Description = "Pullover";
        }
    }
}