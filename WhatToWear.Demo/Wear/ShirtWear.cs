﻿namespace WhatToWear.Demo.Wear
{
    public class ShirtWear : BaseWear
    {
        public ShirtWear()
        {
            Description = "Shirt";
        }
    }
}