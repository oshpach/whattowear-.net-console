﻿namespace WhatToWear.Demo.Wear
{
    public class BootsWear : BaseWear
    {
        public BootsWear()
        {
            Description = "Boots";
        }
    }
}