﻿namespace WhatToWear.Demo.Wear
{
    public class ShortsWear : BaseWear
    {
        public ShortsWear()
        {
            Description = "Shorts";
        }
    }
}