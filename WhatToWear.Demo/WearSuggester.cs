﻿using System;
using WhatToWear.Demo.Definitions;
using WhatToWear.Demo.UserInputHandler;

namespace WhatToWear.Demo
{
    public class WearSuggester
    {
        private readonly IUserInputHandler _userInputHandler;

        public WearSuggester() : this(new UserInputHandler.UserInputHandler())
        {
        }

        public WearSuggester(IUserInputHandler userInputHandler)
        {
            _userInputHandler = userInputHandler;
        }

        public void Start()
        {
            UserInputResult inputResult;
            do
            {
                PrintWetherList();
                inputResult = _userInputHandler.HandleInput(Console.ReadKey().KeyChar);
                Console.WriteLine();

                switch (inputResult.Status)
                {
                    case ResultStatus.Success:
                        Console.Write("You should wear: ");
                        inputResult.Wear.PrintWear();
                        break;
                    case ResultStatus.Error:
                        Console.WriteLine("Unknown weather");
                        break;
                }
                Console.Write("\n\nPress any key to continue ...");
                Console.ReadKey();
            } while (inputResult.Status != ResultStatus.Exit);
        }

        public void PrintWetherList()
        {
            Console.Clear();
            Console.WriteLine("Select weather type:");
            Console.WriteLine("{0} - {1}", (int)WeatherType.Hot, WeatherType.Hot);
            Console.WriteLine("{0} - {1}", (int)WeatherType.Warm, WeatherType.Warm);
            Console.WriteLine("{0} - {1}", (int)WeatherType.Cool, WeatherType.Cool);
            Console.WriteLine("{0} - {1}", (int)WeatherType.Cold, WeatherType.Cold);
            Console.WriteLine("e - Exit");
        }
    }
}