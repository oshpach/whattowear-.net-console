﻿namespace WhatToWear.Demo.Definitions
{
    public enum WeatherType
    {
        Hot = 1,
        Warm = 2,
        Cool = 3,
        Cold = 4
    }
}