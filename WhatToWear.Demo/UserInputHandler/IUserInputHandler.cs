﻿namespace WhatToWear.Demo.UserInputHandler
{
    public interface IUserInputHandler
    {
        UserInputResult HandleInput(char option);
    }
}