﻿using WhatToWear.Demo.Definitions;
using WhatToWear.Demo.Wear;

namespace WhatToWear.Demo.UserInputHandler
{
    public class UserInputResult
    {
        public IWear Wear { get; set; }
        public ResultStatus Status { get; set; }
    }
}