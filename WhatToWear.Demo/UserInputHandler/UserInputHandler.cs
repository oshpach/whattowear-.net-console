﻿using System;
using WhatToWear.Demo.Definitions;
using WhatToWear.Demo.WearFactory;

namespace WhatToWear.Demo.UserInputHandler
{
    public class UserInputHandler : IUserInputHandler
    {
        private readonly IWearFactory _wearFactory;

        public UserInputHandler() : this(new WearFactory.WearFactory())
        {
            
        }

        public UserInputHandler(IWearFactory wearFactory)
        {
            _wearFactory = wearFactory;
        }

        public UserInputResult HandleInput(char option)
        {
            WeatherType weatherType;

            var result = new UserInputResult();
            if (option == Constants.ExitKey)
            {
                result.Status = ResultStatus.Exit;
            }
            else if (Enum.TryParse(option.ToString(), true, out weatherType) && Enum.IsDefined(typeof(WeatherType), weatherType))
            {
                result.Wear = _wearFactory.GetWear(weatherType);
                result.Status =  ResultStatus.Success;
            }
            else
            {
                result.Status = ResultStatus.Error;
            }

            return result;
        }
    }
}