﻿namespace WhatToWear.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            var wearSuggester = new WearSuggester();
            wearSuggester.Start();
        }
    }
}