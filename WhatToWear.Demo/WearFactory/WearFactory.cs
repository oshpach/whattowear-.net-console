﻿using WhatToWear.Demo.Definitions;
using WhatToWear.Demo.Wear;

namespace WhatToWear.Demo.WearFactory
{
    public class WearFactory : IWearFactory
    {
        public IWear GetWear(WeatherType weatherType)
        {
            IWear wear = null;
            switch (weatherType)
            {
                case WeatherType.Hot:
                    wear = new ShirtWear { NextWear = new ShortsWear { NextWear = new SneakersWear() } };
                    break;
                case WeatherType.Warm:
                    wear = new ShirtWear { NextWear = new PantsWear { NextWear = new SneakersWear() } };
                    break;
                case WeatherType.Cool:
                    wear = new JacketWear { NextWear = new ShirtWear { NextWear = new PantsWear { NextWear = new SneakersWear() } } };
                    break;
                case WeatherType.Cold:
                    wear = new HatWear { NextWear = new JacketWear { NextWear = new PulloverWear { NextWear = new PantsWear { NextWear = new BootsWear() } } } };
                    break;
            }
            return wear;
        }
    }
}