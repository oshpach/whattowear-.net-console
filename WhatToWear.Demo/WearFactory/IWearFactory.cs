﻿using WhatToWear.Demo.Definitions;
using WhatToWear.Demo.Wear;

namespace WhatToWear.Demo.WearFactory
{
    public interface IWearFactory
    {
        IWear GetWear(WeatherType weatherType);
    }
}